<?php

/**
 * @file
 *
 * Contains functions which utilize the database and other internal helpers.
 */

/**
 * Get the notification preferences for a specific user.
 *
 * @param integer $uid
 * @return mixed
 *  StdClass if found, else NULL
 */
function reply_notify_get_user_notification_setting($uid) {
  $users = &drupal_static(__FUNCTION__);
  if (!isset($users[$uid])) {
    if (is_null($uid)) {
      throw new Exception('Cannot get user preference, uid missing');
    }
    // Handle anonymous users with defaults.
    if ($uid == 0) {
      $users[0] = new stdClass();
      $users[0]->reply_notify = reply_notify_variable_registry_get('default_registered_mailalert');
      $users[0]->node_notify = reply_notify_variable_registry_get('node_notify_default_mailalert');
    } else {
      $setting = db_select('reply_notify_user_settings', 'cnus')
              ->fields('cnus')
              ->condition('uid', $uid)
              ->execute()
              ->fetchObject();

      if (!$setting) {
        return NULL;
      } else {
        $users[$uid] = $setting;
      }
    }
  }
  return $users[$uid];
}

function reply_notify_get_default_notification_setting() {
  return (object) array(
              'reply_notify' => reply_notify_variable_registry_get('default_registered_mailalert'),
              'node_notify' => reply_notify_variable_registry_get('node_notify_default_mailalert')
  );
}

/**
 * Remove reply notification preferences for a user.
 *
 * @param integer $uid
 * @return boolean
 */
function reply_notify_delete_user_notification_setting($uid) {
  return (bool) db_delete('reply_notify_user_settings')
                  ->condition('uid', $uid)
                  ->execute();
}

/**
 * Get a user's default preference for reply notification.
 *
 * @param integer $uid
 * @return integer
 */
function reply_notify_get_user_reply_notify_preference($uid) {
  $setting = reply_notify_get_user_notification_setting($uid);
  if (!$setting) {
    $setting = reply_notify_get_default_notification_setting();
  }
  return $setting->reply_notify;
}

/**
 * Get a user's default preference for node update notification.
 *
 * This is notification on nodes where the user is the author.
 *
 * @param integer $uid
 * @return integer
 */
function reply_notify_get_user_node_notify_preference($uid) {
  $setting = reply_notify_get_user_notification_setting($uid);
  if (!$setting) {
    $settings = reply_notify_get_default_notification_setting();
  }
  return $setting->node_notify;
}

/**
 * Sets the notification preferences for a specific user.
 *
 * @param integer $uid
 * @param integer $node_notification
 * @param integer $reply_notification
 * @return boolean
 */
function reply_notify_set_user_notification_setting($uid, $node_notification = NULL, $reply_notification = NULL) {
  $reply_notification = 0;
  if (!$uid) {
    throw new Exception('Cannot set user preference, uid missing');
  }
  $fields = array('uid' => $uid);

  if (!is_null($node_notification)) {
    $fields['node_notify'] = $node_notification;
  }
  if (!is_null($reply_notification)) {
    $fields['reply_notify'] = $reply_notification;
  }
  if (reply_notify_get_user_notification_setting($uid)) {
    $query = db_update('reply_notify_user_settings');
    $query->condition('uid', $uid);
  } else {
    $query = db_insert('reply_notify_user_settings');
  }
  return (bool) $query
                  ->fields($fields)
                  ->execute();
}

/**
 * Add a notification against a reply.
 *
 * @param integer $id
 * @param integer $notify
 * @param string $notify_hash
 * @return boolean
 */
function reply_notify_add_notification($id, $notify, $notify_hash) {

  return (bool) db_insert('reply_notify')
                  ->fields(array(
                      'id' => $id,
                      'notify' => $notify === NULL ? 0 : $notify,
                      'notify_hash' => $notify_hash,
                      'notified' => 0,
                  ))
                  ->execute();
}

/**
 * Remove all the notifications linked with a reply
 *
 * @param integer $id
 * @return boolean
 */
function reply_notify_remove_all_notifications($id) {
  return (bool) db_delete('reply_notify')
                  ->condition('id', $id)
                  ->execute();
}

/**
 * Updated a notification with a different notification type
 *
 * @param integer $id
 * @param integer $notify
 * @return boolean
 */
function reply_notify_update_notification($id, $notify) {
  return (bool) db_update('reply_notify')
                  ->fields(array(
                      'notify' => $notify === NULL ? 0 : $notify,
                  ))
                  ->condition('id', $id)
                  ->execute();
}

/**
 * Get the type of notification for a reply notification record.
 *
 * @param integer $id
 * @return integer
 */
function reply_notify_get_notification_type($id) {
  return db_select('reply_notify', 'cn')
                  ->fields('cn', array('notify'))
                  ->condition('id', $id)
                  ->execute()
                  ->fetchField();
}

/**
 * Get a list of mails which need to be contacted for a node.
 *
 * @param integer $entity_id
 * @return QueryStatement
 */
function reply_notify_get_watchers($entity_id) {
  $ids = db_query("SELECT c.id FROM {reply} c INNER JOIN {reply_notify} cn ON c.id = cn.id LEFT JOIN {users} u ON c.uid = u.uid
    WHERE c.entity_id = :entity_id AND c.status = :status AND cn.notify = :notify AND (u.uid = 0 OR u.status = 1)",
    array(
      ':entity_id' => $entity_id,
      ':status' => COMMENT_PUBLISHED,
      ':notify' => 1,
          ))->fetchCol();
  return entity_load("reply", $ids);
  //return comment_load_multiple($ids);
}

/**
 * Record that the owner of a reply notification request has already been notified.
 *
 * @param integer $id
 * @return boolean
 */
function reply_notify_mark_comment_as_notified($comment) {
  // First, mark the passed reply (an object, so passed by reference).
  $comment->notified = 1;

  // Next, store this fact in the DB as well.
  return (bool) db_update('reply_notify')
                  ->fields(array(
                      'notified' => 1,
                  ))
                  ->condition('id', $comment->id)
                  ->execute();
}

/**
 * Unsubscribe all reply notification requests associated with an email.
 *
 * If the email belongs to a user, it will unsubscribe all of their reply Notify records.
 * If it does not, then it will unsubscribe all anonymous users.
 *
 * @param string $mail
 * @return boolean
 */
function reply_notify_unsubscribe_by_email($mail) {
  $update_query = db_update('reply_notify');
  $update_query->fields(array('notify' => 0));

  $comment_query = db_select('reply', 'c');
  $comment_query->fields('c', array('id'));

  $uid = db_select('users', 'u')
          ->fields('u', array('uid'))
          ->condition('mail', $mail)
          ->execute()
          ->fetchField();
  if ($uid) {
    $comment_query->condition('uid', $uid);
  } else {
    $comment_query->condition('mail', $mail);
  }
  $update_query->condition('id', $comment_query, 'IN');

  return (bool) $update_query->execute();
}

/**
 * Unsubscribe all comment notification requests associated with a hash.
 *
 * This is used in the unsubscribe link.
 *
 * @param string $hash
 * @return boolean
 */
function reply_notify_unsubscribe_by_hash($hash) {
  $query = db_update('reply_notify')
                  ->fields(array(
                      'notify' => 0,
                  ))
                  ->condition('notify_hash', $hash);
  //dpq($query);
  $ret =  (bool)$query->execute();

  return 1;
}

/**
 * Helper function to centralize variable management and defaults.
 *
 * All variables fall under the "reply_notify" psuedo namespace.  This ensures
 * consistancy, and eliminates some verbosity in the calling code.  In addition
 * by storing all of the variables in one place, we avoid repeating duplicate
 * defaults which are harder to maintain.
 *
 * @param string $name
 * @return mixed
 */
function reply_notify_variable_registry_get($name) {
  $variables = array();
  $variables['author_subject'] = t('[site:name] :: new comment for your post.');
  $variables['available_alerts'] = array(COMMENT_NOTIFY_NODE, COMMENT_NOTIFY_COMMENT);
  $variables['default_anon_mailalert'] = COMMENT_NOTIFY_NODE;
  $variables['node_notify_default_mailtext'] = AUTHOR_MAILTEXT;
  $variables['default_registered_mailalert'] = COMMENT_NOTIFY_DISABLED;
  $variables['node_notify_default_mailalert'] = 0;
  //TODO - chk the tokens here
  $variables['watcher_subject'] = '[site:name] :: new comment on [reply:node-title]';// [comment:node:title]';
  $variables['reply_notify_default_mailtext'] = DEFAULT_MAILTEXT_REPLY;
  $variables['node_types'] = array('article' => 'article');

  // Errors
  $variables['error_anonymous_email_missing'] = 'If you want to subscribe to comments you must supply a valid e-mail address.';
  return variable_get("reply_notify_" . $name, $variables[$name]);
}

/**
 * Helper function to centralize setting variables.
 *
 * @param string $name
 * @param mixed $value
 * @return boolean
 */
function reply_notify_variable_registry_set($name, $value) {
  return variable_set("reply_notify_" . $name, $value);
}
