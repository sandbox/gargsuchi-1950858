<?php

/**
 * @file
 * Builds placeholder replacement tokens for reply_notify.module.
 */

/**
 * Implements hook_token_info().
 */
function reply_notify_token_info() {
  // Comment tokens.
  $info['tokens']['reply']['unsubscribe-url'] = array(
      'name' => t('Unsubscribe URL'),
      'description' => t('The URL to disable notifications for the comment.'),
      'type' => 'url',
  );

  $info['tokens']['reply']['author'] = array(
      'name' => t("Author"),
      'description' => t("The author of the comment, if they were logged in."),
      'type' => 'user',
  );
  $info['tokens']['reply']['node-title'] = array(
      'name' => t("Node Title"),
      'description' => t("The title of the entity on which comment was made."),
      'type' => 'user',
  );
  $info['tokens']['reply']['body'] = array(
      'name' => t("Comment Body"),
      'description' => t("The body of the comment."),
      'type' => 'user',
  );
  $info['tokens']['reply']['URL'] = array(
      'name' => t("Comment URL"),
      'description' => t("The URL of the comment."),
      'type' => 'user',
  );

  return $info;
}

/**
 * Implements hook_tokens().
 */
function reply_notify_tokens($type, $tokens, array $data = array(), array $options = array()) {
  global $base_url;
  $url_options = array('absolute' => TRUE);
  if (isset($options['language'])) {
    $url_options['language'] = $options['language'];
    $language_code = $options['language']->language;
  } else {
    $language_code = NULL;
  }
  $sanitize = !empty($options['sanitize']);

  $replacements = array();

  if ($type == 'reply' && !empty($data['comment'])) {
    $comment = $data['comment'];
    $comment_sub = $data['comment-subscribed'];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'unsubscribe-url':
          if ($unsubscribe_url = reply_notify_get_unsubscribe_url($comment_sub)) {
            $replacements[$original] = url($unsubscribe_url, $url_options);
          }
          break;
        // Default values for the chained tokens handled below.
        case 'author':
          if ($comment->uid > 0) {
            $u = user_load($comment->uid);
            $name = $u->name;
          } else {
            $u = $comment->field_first_name['und'][0]['value'];
          }
          $replacements[$original] = $sanitize ? filter_xss($name) : $name;
          break;
        case 'node-title':
          $entity_id = $comment->entity_id;
          $entity = entity_load('field_collection_item', array($entity_id));
          $node_title = $entity[$entity_id]->field_title['und'][0]['value'];
          
          $replacements[$original] = $sanitize ? filter_xss($node_title) : $node_title;
          break;
        case 'body':
          $body = $comment->field_comment['und'][0]['value'];

          $replacements[$original] = $sanitize ? filter_xss($body) : $body;
          break;
        case 'url':
          $entity_id = $comment->entity_id;
          $path = drupal_get_path_alias("field-collection/field-photos/" . $entity_id);
          $abs_path = $base_url . "/" . $path;
          
          $replacements[$original] = $sanitize ? filter_xss($abs_path) : $abs_path;
          break;
      }
    }
  }

  return $replacements;
}
